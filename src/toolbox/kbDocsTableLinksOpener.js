import browser from "webextension-polyfill"

function getColumn(tab, col) {
  var n = tab.rows.length;
  var i;
  var s = [];
  var tr;
  var td;

  // First check that col is not less then 0
  if (col < 0) {
    return null;
  }

  for (i = 0; i < n; i++) {
    tr = tab.rows[i];
    if (tr.cells.length > col) {
      // Check that cell exists before you try
      td = tr.cells[col]; // to access it.
      s.push(td.querySelector("a").href);
    } // Here you could say else { return null; } if you want it to fail
    // when requested column is out of bounds. It depends.
  }
  return s;
}

export default async function() {
  const table = document.querySelector("#docsTable");
  if (table) {
    return {
      name: "KB Doc Table Links Opener",
      action: async () => {
        const links = getColumn(table, 0);
        return Promise.all(
          links.map(link =>
            browser.runtime.sendMessage(null, {
              type: "openBackgroundTab",
              payload: link
            })
          )
        );
      }
    };
  }
};
