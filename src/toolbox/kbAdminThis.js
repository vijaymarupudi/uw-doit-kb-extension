import browser from "webextension-polyfill"

function kbAdminLink() {
  const urlSearchParams = new URLSearchParams(window.location.search);
  const kbid = urlSearchParams.get("id");
  if (kbid) {
    return `https://kb.wisc.edu/kbAdmin/document.php?id=${kbid}`;
  }
}

export default function() {
  const url = kbAdminLink();
  if (url && !window.location.href.startsWith("https://kb.wisc.edu/kbAdmin")) {
    return {
      name: "KB Admin Doc Link",
      action: () => {
        browser.runtime.sendMessage(null, {
          type: "openTab",
          payload: url
        });
      }
    };
  }
};
