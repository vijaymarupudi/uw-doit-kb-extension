import browser from "webextension-polyfill"

function getExternalKbLink() {
  const $externalSitesHeader = Array.from(document.querySelectorAll("th")).find(
    el => el.textContent === "Sites:"
  )
  
  if (!$externalSitesHeader) return;

  const $externalSites = $externalSitesHeader.nextSibling;
  const $externalLink = $externalSites.querySelector("a");

  if ($externalLink) {
    return $externalLink.getAttribute("href"); 
  }
}

export default function() {
  const externalKbLink = getExternalKbLink();
  if (externalKbLink) {
    return {
      name: 'Copy External KB Link',
      action: async () => {
        browser.runtime.sendMessage(null, {
          type: 'createNotification',
          payload: `Copied to clipboard: ${externalKbLink}`
        })
        return navigator.clipboard.writeText(externalKbLink)
      }
    }
  } 
};
