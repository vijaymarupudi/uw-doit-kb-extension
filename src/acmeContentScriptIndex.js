function objectToFormData(object) {
  const formData = new FormData();
  for (const [key, value] of Object.entries(object)) {
    formData.append(key, value);
  }
  return formData;
}

function download(filename, text, mimetype) {
  if (!mimetype) throw Error("mimetype required!");
  const element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:" + mimetype + ";charset=utf-8," + encodeURIComponent(text)
  );
  element.setAttribute("download", filename);

  element.style.display = "none";
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

function parseHTMLToDocument(htmlString) {
  const parser = new DOMParser();
  const newDocument = parser.parseFromString(htmlString, "text/html");
  return newDocument;
}

async function getUserData(el) {
  const user = el.dataset.id;
  const grid = el.dataset.grid;
  const hours = document.getElementById(`${user}_hours`).innerText;

  const postData = {
    grid: grid,
    load_user: user,
    action: "adminloadschedule",
    avail_index: 0,
    hours: hours
  };

  const resp = await fetch(
    "https://acme.wisc.edu/tools/schedule/admin/post/admin_avail_post.php",
    {
      method: "POST",
      body: objectToFormData(postData)
    }
  );

  const availabilityData = await resp.text();
  return availabilityData;
}

async function getCSVData(element) {
  const user = element.dataset.id;
  const fourLetter = document.getElementById(`${user}_name`).nextElementSibling
    .innerText;
  const rawHTML = await getUserData(element);
  const parsingDoc = parseHTMLToDocument(rawHTML);
  const rows = parsingDoc.querySelectorAll("tr.row");
  const csvArrayOfElements = Array.from(rows).map(rowEl =>
    rowEl.querySelectorAll('td:not([class="row_label"])')
  );
  const csvArray = csvArrayOfElements.map(row => {
    return Array.from(row).map(detail => {
      const divInDetail = detail.querySelector("div");
      const classString = divInDetail.getAttribute("class");
      if (classString.includes("selection_on")) {
        return "G";
      } else if (classString.includes("selection_off")) {
        return "R";
      } else if (classString.includes("selection_maybe")) {
        return "Y";
      } else {
        return "C";
      }
    });
  });
  return [fourLetter, csvArray];
}

async function addAvailabilityGridDownloadButton() {
  const downloadButton = document.createElement("button");
  downloadButton.innerText = "Download as JSON";
  downloadButton.addEventListener("click", async e => {
    e.target.innerText = "Loading...";
    e.target.disabled = true;
    const availabilityElements = Array.from(document.querySelectorAll(".load"));
    const unprocessedCSVDatum = await Promise.all(
      availabilityElements.map(getCSVData)
    );

    const processedCSVDatum = unprocessedCSVDatum.reduce(
      (acc, [fourLetter, csvArray]) => {
        acc[fourLetter] = csvArray;
        return acc;
      },
      {}
    );

    e.target.disabled = false;
    e.target.innerText = "Download as JSON";
    download(
      "schedules.json",
      JSON.stringify(processedCSVDatum, null, 2),
      "application/json"
    );
  });

  document
    .querySelector("#delete")
    .parentElement.insertBefore(downloadButton, null);
}

(async function() {
  const url = window.location.pathname;
  if (url === "/tools/schedule/admin/admin_avail.php") {
    addAvailabilityGridDownloadButton();
  }
})();
