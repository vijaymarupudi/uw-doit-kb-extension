const path = require('path')

module.exports = {
  mode: "development",
  entry: {
    kbContentScript: "./kbContentScriptIndex.js",
    pageScript: "./pageScript/index.js",
    backgroundScript: "./backgroundScriptIndex.js",
    acmeContentScript: "./acmeContentScriptIndex.js"
  },

  output: {
    path: path.resolve(path.join(__dirname, '..', 'build'))
  },

  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  },
  devtool: "source-map"
};
