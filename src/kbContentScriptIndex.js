import externalKbLink from "./toolbox/externalKbLink";
import kbAdminThis from "./toolbox/kbAdminThis";
import kbDocsTableLinkOpener from "./toolbox/kbDocsTableLinksOpener";
import browser from "webextension-polyfill";

async function main() {
  document.addEventListener("keydown", e => {
    if (e.key === "s" && (e.ctrlKey || e.metaKey)) {
      e.preventDefault();
      browser.runtime.sendMessage(null, {
        type: "openSearcher"
      });
    }
  });

  const actionfunctions = [externalKbLink, kbAdminThis, kbDocsTableLinkOpener];
  const actionObjects = await Promise.all(actionfunctions.map(func => func()));
  const filteredActionObjects = actionObjects.filter(item => item);

  if (filteredActionObjects.length === 0) return;

  const $toolbar = document.createElement("div");
  $toolbar.setAttribute("id", "vijay-toolbar");

  filteredActionObjects.forEach(obj => {
    const $button = document.createElement("button");
    $button.innerText = obj.name;
    $button.addEventListener("click", obj.action);
    $toolbar.append($button);
  });

  const preExistingToolbar =
    document.querySelector("#top-links-int") || // for user side kb
    document.querySelector('header[role="banner"]');
  preExistingToolbar.parentElement.insertBefore($toolbar, preExistingToolbar);
}

main();
