import React, {useLayoutEffect, useRef} from 'react'

export default function ResultsViewer({ results, selectedIndex }) {

  const selectedItemRef = useRef(null);

  // When selection has changed, scroll into view
  useLayoutEffect(() => {
    if (selectedItemRef.current) {
      selectedItemRef.current.scrollIntoView({
        block: "nearest"
      });
    }
  }, [selectedIndex]);

  return (
    <div>
      <table className="table is-narrow is-fullwidth is-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Hits</th>
          </tr>
        </thead>
        <tbody>
          {results.map((result, idx) => {
            const selected = selectedIndex === idx;

            const resultDisplayElement = (
              <span dangerouslySetInnerHTML={{ __html: result.title }} />
            );

            return (
              <tr
                key={result.id}
                className={selected ? "is-selected" : undefined}
                ref={selected ? selectedItemRef : undefined}
              >
                <td>{idx + 1}</td>
                <td>
                  <a href={result.url} target="_blank">
                    {result.priority < 3 ? (
                      <b>{resultDisplayElement}</b>
                    ) : (
                      resultDisplayElement
                    )}
                  </a>
                </td>
                <td>{result.hits}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
