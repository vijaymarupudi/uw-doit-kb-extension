import React, { useState, useEffect, useRef, useCallback } from "react";
import ReactDOM from "react-dom";
import _ from "lodash";
import browser from "webextension-polyfill";
import ResultsViewer from "./ResultsViewer";

function urlForSearch(term) {
  const urlParams = new URLSearchParams();
  urlParams.append("q", term);
  urlParams.append("format", "json");
  urlParams.append("limit", "1000");
  return `https://kb.wisc.edu/helpdesk/internal/search.php?${urlParams.toString()}`;
}

function searchKB(term) {
  const controller = new AbortController();

  function cleanup() {
    controller.abort();
  }

  const request = fetch(urlForSearch(term), {
    signal: controller.signal
  })
    .then(resp => resp.json())
    .then(jsonData => jsonData.results);

  return [request, cleanup];
}

function Header() {
  return (
    <section className="hero is-primary">
      <div className="hero-body">
        <div className="container">
          <h1 className="title">Vijay's KB Searcher</h1>
        </div>
      </div>
    </section>
  );
}

function useSelectedIndex(max, dependents) {
  const [selectedIndex, setSelectedIndex] = useState(0);

  const maxRef = useRef();
  maxRef.current = max;

  const changeSelectedIndex = useCallback(function(offset) {
    setSelectedIndex(function(currentIndex) {
      const newIndex = currentIndex + offset;
      if (newIndex >= 0 && newIndex < maxRef.current) {
        return newIndex;
      } else {
        return currentIndex;
      }
    });
  }, []);

  const upDownHandler = useCallback(function(e) {
    switch (e.key) {
      case "ArrowUp":
        changeSelectedIndex(-1);
        break;
      case "ArrowDown":
        changeSelectedIndex(1);
        break;
    }
  }, [changeSelectedIndex]);

  useEffect(() => {
    document.addEventListener("keydown", upDownHandler);

    return () => {
      document.removeEventListener("keydown", upDownHandler);
    };
  }, []);

  // Reset when dependents change.
  useEffect(() => {
    return () => {
      setSelectedIndex(0);
    };
  }, dependents);

  return selectedIndex;
}

function useSearch(searchTerm) {
  const [displayedSearchResults, setDisplayedSearchResults] = useState(null);
  const cleanupRef = useRef();

  useEffect(() => {
    if (searchTerm) {
      const [ongoingRequest, cleanupFunction] = searchKB(searchTerm);
      cleanupRef.current = cleanupFunction;
      setDisplayedSearchResults(null);
      ongoingRequest.then(setDisplayedSearchResults);
    }

    return () => {
      if (cleanupRef.current) cleanupRef.current();
    };
  }, [searchTerm]);

  return displayedSearchResults;
}

function useClearAndFocus(searchInputRef) {

  // focus on start
  useEffect(() => {
    searchInputRef.current.focus();
  }, []);

  const clearAndFocus = useCallback(function() {
    if (searchInputRef.current) {
      searchInputRef.current.focus();
      searchInputRef.current.scrollIntoView();
      searchInputRef.current.select();
    }
  }, [searchInputRef]);

  const shortcutListener = useCallback(function(e) {
    if (e.key === "s" && (e.ctrlKey || e.metaKey)) {
      e.preventDefault();
      clearAndFocus();
    }
  }, [clearAndFocus]);

  const clearAndFocusListener = useCallback(function(message) {
    if (message.type === "clearAndFocus") {
      clearAndFocus();
    }
  }, [clearAndFocus]);

  useEffect(() => {
    document.addEventListener("keydown", shortcutListener);
    browser.runtime.onMessage.addListener(clearAndFocusListener);

    return () => {
      document.removeEventListener("keydown", shortcutListener);
      browser.runtime.onMessage.removeListener(clearAndFocusListener);
    };
  }, []);
}

function useEnter(searchTerm, results, selectedIndex) {
  const searchTermRef = useRef();

  searchTermRef.current = searchTerm;

  const enterCallback = useCallback(function(e) {
    if (e.key === "Enter") {
      const kbDocNumber = parseInt(searchTermRef.current);
      if (!isNaN(kbDocNumber)) {
        window.open(
          `https://kb.wisc.edu/helpdesk/internal/page.php?id=${kbDocNumber}`,
          ""
        );
      } else {
        window.open(results[selectedIndex].url, "");
      }
    }
  }, [searchTermRef, results, selectedIndex]);

  useEffect(() => {
    document.addEventListener("keydown", enterCallback);

    return () => {
      document.removeEventListener("keydown", enterCallback);
    };
  }, [enterCallback]);
}

function App() {
  const [displayedSearchTerm, setDisplayedSearchTerm] = useState("");

  function handleSearchInputChange(e) {
    const term = e.target.value;
    setDisplayedSearchTerm(term);
  }

  // clearing and focusing
  const searchInputRef = useRef(null);
  useClearAndFocus(searchInputRef);

  // searching
  const displayedResults = useSearch(displayedSearchTerm);

  // selection browsing
  const selectedIndex = useSelectedIndex(
    displayedResults && displayedResults.length,
    [displayedSearchTerm]
  );

  // handling entering
  useEnter(displayedSearchTerm, displayedResults, selectedIndex);

  return (
    <div className="container">
      <Header />
      <div className="section">
        <div className="field">
          <label className="label">Search</label>
          <div className="control">
            <input
              className="input"
              type="text"
              placeholder="Search"
              onChange={handleSearchInputChange}
              value={displayedSearchTerm}
              ref={searchInputRef}
            />
            {!displayedResults && (
              <progress className="progress is-small is-primary" max={100} />
            )}
          </div>
        </div>
      </div>
      <div>
        {displayedResults && (
          <ResultsViewer
            results={displayedResults}
            selectedIndex={selectedIndex}
          />
        )}
      </div>
      <div className="section content">
        <ul>
          <li>Ctrl - S: Focus input for new search.</li>
          <li>Up and Down arrows to browse, Enter to visit.</li>
          <li>Type number and hit enter for precise kb doc.</li>
        </ul>
      </div>
    </div>
  );
}

ReactDOM.render(<App />, document.querySelector("#root"));
