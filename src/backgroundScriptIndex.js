import browser from "webextension-polyfill"

async function openTab(link, isBackground) {
  return browser.tabs.create({
    active: !isBackground,
    url: link
  });
}

async function notify(message) {
  return browser.notifications.create(null, {
    type: "basic",
    title: "Vijay's KB Extension",
    message: message,
    iconUrl: "icons/open-book.png"
  });
}

// For clicking page action
let searcherTabID = null;
async function openSearcher() {
  const urlToSearchPage = browser.runtime.getURL("searchPage.html");
  if (searcherTabID) {
    // Switch to tab and focus.
    await browser.tabs.update(searcherTabID, {
      active: true
    });
    await browser.tabs.sendMessage(searcherTabID, {
      type: "clearAndFocus"
    });
  } else {
    // Create new tab and update searcherTabID
    const tab = await browser.tabs.create({
      url: urlToSearchPage
    });
    searcherTabID = tab.id;
    browser.tabs.onRemoved.addListener(tabID => {
      if (tabID === searcherTabID) {
        searcherTabID = null;
      }
    });
  }
}

browser.browserAction.onClicked.addListener(openSearcher);

// For communication from content scripts
browser.runtime.onMessage.addListener(message => {
  switch (message.type) {
    case "openBackgroundTab":
      return openTab(message.payload, true);
    case "openTab":
      return openTab(message.payload);
    case "createNotification":
      return notify(message.payload);
    case "openSearcher":
      return openSearcher();
  }
});
