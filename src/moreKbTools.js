function getCheckboxWithLabel(labelText) {
  const $label = Array.from(document.querySelectorAll("label")).find(
    el => el.innerText.includes(labelText)
  );
  const optionID = $label.getAttribute("for");
  const $checkbox = document.querySelector(`#${optionID}`);
  return $checkbox;
}

function setInProgress() {
  const $checkbox = getCheckboxWithLabel(
    "up and set to 'In Progress'"
  );
  const $comment = document.querySelector('#comment')
  $comment.value = prompt('Type in reason for editing')
  $checkbox.checked = true;
}

setInProgress()