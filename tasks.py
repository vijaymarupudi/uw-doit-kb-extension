#!/usr/bin/env python
"""
The task file for invoke (inv).
"""

from invoke import task

@task
def build(c, dev=False):
    c.run(f"""
    mkdir -p build

    cd src
    NODE_ENV={'development' if dev else 'production'} npx webpack {'' if dev else '-p'}
    cd ..

    rsync public/ build/ -a
    cd build
    web-ext build
    """)

@task
def watch(c):
    c.run(f"""
    mkdir -p build
    cd src
    npx webpack --watch --quiet &
    cd ../
    echo 'Watching files'
    fd | entr -s 'rsync public/ build/ -a && echo "Synced files!"' &
    cd build
    web-ext run --verbose
    """)

@task(build)
def deploy(c):
    c.run("""
    cd build
    web-ext sign --api-key $MOZ_API_KEY --api-secret $MOZ_API_SECRET
    """)

@task
def clean(c):
    c.run("rm build -r")
